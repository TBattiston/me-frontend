import React from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import './App.css';
import Navbar from './components/Navbar';
import SkillList from './components/SkillList';
import WorkExperienceList from './components/WorkExperienceList';

function App() {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <Navbar />
          <Switch>
            <Route exact path="/">
              <SkillList />
            </Route>

            <Route path="/workExperiences">
              <WorkExperienceList />
            </Route>
          </Switch>
        </header>
      </div>
    </Router>
  );
}

export default App;
