export async function fetchSkills() {
  return fetch('/api/skills').then(response => response.json());
}

export async function fetchWorkExperiences() {
  return fetch('/api/work_experiences').then(response => response.json());
}
