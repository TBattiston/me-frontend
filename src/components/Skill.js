import React from 'react';
import './Skill.css';

function Skill({ name, level }) {
  return (
    <article className="Skill">
      <header>{name}</header>
      <span className={level}>{level}</span>
    </article>
  );
}

export default Skill;
