import React, { useEffect, useState } from 'react';
import { fetchSkills } from '../api';
import Skill from './Skill';
import './SkillList.css';

function SkillList() {
  const [skills, setSkills] = useState([]);
  useEffect(() => {
    fetchSkills().then(data => setSkills(data));
  }, []);

  return (
    <div className="SkillList">
      <div className="SkillList-inner">
        {skills.map(skill => <Skill key={skill.name} name={skill.name} level={skill.level} />)}
      </div>
    </div>
  );
}

export default SkillList;
