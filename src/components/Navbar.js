import React from 'react';

import { NavLink } from "react-router-dom";

import './Navbar.css';

function Navbar() {
  return (
    <nav className="Navbar">
      <NavLink exact to="/">Skills</NavLink>

      <NavLink to="/workExperiences">Work Experiences</NavLink>
    </nav>
  );
}

export default Navbar;
