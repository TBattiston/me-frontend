import React from 'react';

function WorkExperienceTiming({ startDate, endDate }) {
  const start = new Date(startDate);
  const end = endDate ? new Date(endDate) : null;

  return (
    <p className="WorkExperience-timing">
      {end ? `From ${formatDate(start)} to ${formatDate(end)}` : `Since ${formatDate(start)}`}
    </p>
  );
}

function formatDate(date) {
  return `${date.toLocaleString('default', { month: 'long' })} ${date.getFullYear()}`
}

export default WorkExperienceTiming;
