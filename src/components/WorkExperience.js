import React from 'react';
import parse from 'html-react-parser';
import './WorkExperience.css';
import WorkExperienceTiming from './WorkExperienceTiming';

function WorkExperience({ company, position, description, startDate, endDate, skills }) {
  return (
    <article className="WorkExperience">
      <header>
        <h3>{company}</h3>
        <span>{position}</span>
      </header>
      <WorkExperienceTiming startDate={startDate} endDate={endDate} />
      <h4>Assignations</h4>
      <p className="WorkExperience-description">
        {parse(description.replace(/\n/g, "<br />"))}
      </p>

      <div className="WorkExperience-skills">
        {skills.map(skill => <span key={skill.name} className="WorkExperience-skill">{skill.name}</span>)}
      </div>
    </article>
  );
}

export default WorkExperience;
