import React, { useEffect, useState } from 'react';
import { fetchWorkExperiences } from '../api';
import WorkExperience from './WorkExperience';
import './WorkExperienceList.css';

function WorkExperienceList() {
  const [workExperiences, setWorkExperiences] = useState([]);
  useEffect(() => {
    fetchWorkExperiences().then(data => setWorkExperiences(data));
  }, []);

  return (
    <div className="WorkExperienceList">
      <div className="WorkExperienceList-inner">
        {workExperiences.map(workExperience =>
          <WorkExperience
            key={workExperience.company}
            position={workExperience.position}
            company={workExperience.company}
            description={workExperience.description}
            skills={workExperience.skills}
            startDate={workExperience.start_date}
            endDate={workExperience.end_date}
          />
        )}
      </div>
    </div>
  );
}

export default WorkExperienceList;
